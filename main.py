import re
import subprocess
import sys
import platform
import urllib.request as urllib2
import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

MY_ADDRESS = 'mymail@gmail.com'
PASSWORD = '*****'

####
#Check if a Java Version in installed
#Return the Java Version,
#  Return 1 if no version is installed
def getJavaVersion():
    retCode = subprocess.getstatusoutput("java -version")[0]
    if retCode==0:
        try:
            result = str(subprocess.run(["java", "-version"],
                                capture_output=True))
        except subprocess.CalledProcessError as e:
            print(f'ERRORE : {e.output}')


        pattern = '\"(\d+).*\"'
        return re.search(pattern, result).groups()[0]
    else:
        return 1


####
# Get TAR file on oracle website, depending of the version
def getJDKLinux_GET(url, filename):
    cookie = 'Cookie'
    license = 'oraclelicense=accept-securebackup-cookie'
    opener = urllib2.build_opener()
    opener.addheaders.append((cookie, license))
    print('Download starting. File name : ', filename)
    f = opener.open(url)
    with open(filename, 'wb+') as save:
        save.write(f.read())
    print('Download complete. File name : ', filename)

####
# Decompress the TAR file with JAVA sources, then set JAVA_HOME
def installOnLinux(filename):
    print('Installing on Linux :', filename)

    subprocess.run(["tar", "zxvf", filename])
    betterName = filename.split("_")[0]
    subprocess.run(["rm", filename])

    ##TODO : add $JAVA_HOME

####
# Add linuxprising java repository, then install the java version
def getJDKLinux_PPA(version):

    subprocess.run(["add-apt-repository", "ppa:linuxuprising/java"])
    subprocess.run(["apt", "update"])
    ##Pre-validate the oracle certificate asked during installation
    subprocess.run(
        "echo oracle-java14-installer shared/accepted-oracle-license-v1-2 select true | /usr/bin/debconf-set-selections",
        shell=True)
    if version=="11":
        subprocess.run(["apt-get", "install", "-y", "oracle-java11-installer-local"])
    else:
        subprocess.run(["apt-get", "install", "-y", "oracle-java"+version+"-installer"])


####
# Get .EXE file on oracle website, depending of the version
def getJDKWindows_GET(url, filename):
    cookie = 'Cookie'
    license = 'oraclelicense=accept-securebackup-cookie'
    opener = urllib2.build_opener()
    opener.addheaders.append((cookie, license))
    print('Download starting. File name : ', filename)
    f = opener.open(url)
    with open(filename, 'wb+') as save:
        save.write(f.read())
    print('Download complete. File name : ', filename)

####
# Install Java version downloaded, then set environment variable
def installOnWindows(filename):
    print('Installing on Windows :', filename)
    folderName="C:\Program Files\Java\\" + filename.split("_")[0]

    subprocess.run([filename, "/s"])
    print('New Version installed')
    print('Adding ' + folderName + ' to the PATH')
    subprocess.run(["setx", "path", "\"%PATH%;" + folderName + "\\bin\\\""])

    print('Setting JAVA_HOME')
    subprocess.run(["setx", "-m", "JAVA_HOME", folderName])

    subprocess.run(["rm", filename])




####
# Send a simple email to notify the update of the java version
def sendMailNotification(message):
    #set up the SMTP server
    s = smtplib.SMTP(host='smtp.gmail.com', port=587)
    s.starttls()
    s.login(MY_ADDRESS, PASSWORD)

    # For each contact, send the email:
    msg = MIMEMultipart()  # create a message


        # setup the parameters of the message
    msg['From'] = MY_ADDRESS
    msg['To'] = 'limmilkki@gmail.com'
    msg['Subject'] = "This is TEST"

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))
    # send the message via the server set up earlier.
    s.send_message(msg)
    del msg

    # Terminate the SMTP session and close the connection
    s.quit()
    print('Email notification has been sent')


if __name__ == '__main__':

##Didn't achieve to automated this
##JDK8 and JDK11 need Oracle login
    jdks_url_linux= {}
    #jdks_url_linux["8"] = ["https://download.oracle.com/otn/java/jdk/8u261-b12/a4634525489241b9a9e1aa73d9e118e6/jdk-8u261-linux-x64.tar.gz", "jdk-8u261-linux-x64.tar.gz"]
    #jdks_url_linux["11"] = ["https://download.oracle.com/otn/java/jdk/11.0.8%2B10/dc5cf74f97104e8eac863698146a7ac3/jdk-11.0.8_linux-x64_bin.tar.gz", "jdk-11.0.8_linux-x64_bin.tar.gz"]
    jdks_url_linux["14"] = ["https://download.oracle.com/otn-pub/java/jdk/14.0.2+12/205943a0976c4ed48cb16f1043c5c647/jdk-14.0.2_linux-x64_bin.tar.gz", "jdk-14.0.2_linux-x64_bin.tar.gz"]
    jdks_url_linux["15"] = ["https://download.oracle.com/otn-pub/java/jdk/15+36/779bf45e88a44cbd9ea6621d33e33db1/jdk-15_linux-x64_bin.tar.gz", "jdk-15_linux-x64_bin.tar.gz"]

    jdks_url_windows= {}
    jdks_url_windows["14"] = ["https://download.oracle.com/otn-pub/java/jdk/14.0.2+12/205943a0976c4ed48cb16f1043c5c647/jdk-14.0.2_windows-x64_bin.exe", "jdk-14.0.2_windows-x64_bin.exe"]
    jdks_url_windows["15"] = ["https://download.oracle.com/otn-pub/java/jdk/15+36/779bf45e88a44cbd9ea6621d33e33db1/jdk-15_windows-x64_bin.exe", "jdk-15_windows-x64_bin.exe"]






    if len(sys.argv)!=2:
        print('One argument expected. Got ', len(sys.argv)-1)
        sys.exit()

    platform = platform.system()
    if platform=="Linux":
        available_versions= ["11", "14", "15"]
    elif platform=="Windows":
        available_versions = ["14", "15"]
    else:
        available_versions = []
    desiredVersion = sys.argv[1]
    #if desiredVersion not in jdks_url_linux:
        #print('Unknown version of Java, please choose between : ', jdks_url_linux.keys())
        #sys.exit(1)
    if desiredVersion not in available_versions:
        print('This Java Version is not available. Choose between : ', available_versions)
        sys.exit(1)



    version = getJavaVersion()

    if version==1:
        print('No Java Version found...\nInstalling Java Version ', desiredVersion)
    else:
        print(f'Current Java Version : {version}')
        if version==desiredVersion:
            print('Desired version already installed')
        else:
            print('Update Java to desired version :', desiredVersion)

            if platform=="Linux":
                ####
                # First option is to dowload file, then install
                #
                #getJDKLinux_GET(jdks_url_linux[desiredVersion][0], jdks_url_linux[desiredVersion][1])
                #installOnLinux(jdks_url_linux[desiredVersion][1])
                ####


                ####
                # Second option is to use linuxuprising repo to install
                # ( https://www.linuxuprising.com/2020/03/how-to-install-oracle-java-14-jdk14-on.html )
                # I prefer this option cause i don't need to update downloads links
                getJDKLinux_PPA(desiredVersion)

                updatedVersion = getJavaVersion()

                print(f'New Java version :', updatedVersion)
                print('If version hasn\'t changed, maybe you already have other Java version installed.\nCondider using \'update-alternatives --config java\' to select the good one');
            elif platform=="Windows":

                getJDKWindows_GET(jdks_url_windows[desiredVersion][0], jdks_url_windows[desiredVersion][1])
                installOnWindows(jdks_url_windows[desiredVersion][1])

                updatedVersion = getJavaVersion()
                print(f'New Java version :', updatedVersion)
                print('If version hasn\'t changed, consider reboot to update PATH variable environment')
            else:
                print('Unknown platform :', platform)
                exit(1)

    ##TODO ajouter un ID (IP?) pour identifier la machine
    sendMailNotification("The Java Version has been updated from " + version+" to " + desiredVersion + " on the machine "+ platform)



